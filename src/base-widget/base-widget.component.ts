import { Component} from '@angular/core';
import {Http} from '@angular/http';

@Component({
  selector: 'app-text',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent {
  
  public topikBerita: Array<any> = 
  [
      {
        "description": "Lorem ipsum dolor sit ametasdasdasdasdawadaweaddasdwaawdadadaawdwawea"
      },
      {
        "description": "Ut enim ad minim veniam."
      },
      {
        "description": "Lorem ipsum dolor sit amet."
      },
      {
        "description": "Ut enim ad minim veniam."
      },
      {
        "description": "Lorem ipsum dolor sit amet."
      },
      {
        "description": "Ut enim ad minim veniam"
      }
  ];
  
 constructor() {
    console.log(this.topikBerita);
  }

  ngOnInit() {
  }

}
