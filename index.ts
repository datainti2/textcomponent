import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetComponent } from './src/base-widget/base-widget.component';



@NgModule({
  imports: [
    CommonModule,
    BaseWidgetComponent
  ],
  declarations: [
    SampleComponent,
    SampleDirective,
    SamplePipe,
    BaseWidgetComponent
  ],
  exports: [
    SampleComponent,
    SampleDirective,
    SamplePipe
  ]
})
export class TextComponentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TextComponentModule,
      providers: []
    };
  }
}
